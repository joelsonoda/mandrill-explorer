/*
 * Angular 2 decorators and services
 */
import { Component, ViewEncapsulation } from '@angular/core';
import { Routes, Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router';
import {HTTP_PROVIDERS} from '@angular/http';
import {LOG_LOGGER_PROVIDERS, Level, Logger} from "angular2-logger/core";

import { Settings, MandrillSettings } from './settings';
import { Search } from './search';
import { Message } from './message';
import { Home } from './home';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  pipes: [ ],
  providers: [ MandrillSettings, HTTP_PROVIDERS, LOG_LOGGER_PROVIDERS, ROUTER_PROVIDERS ],
  directives: [ ROUTER_DIRECTIVES ],
  encapsulation: ViewEncapsulation.None,
  styles: [
    require('normalize.css'),
    require('./app.css')
  ],
  template: `
    <md-content>
      <md-toolbar color="primary">
          <span>{{ name }}</span>
          <span class="fill"></span>
          <button md-button (click)="router.navigate(['/settings'])">
            Settings
          </button>
          <button md-button (click)="router.navigate(['/search'])">
            Search
          </button>
      </md-toolbar>

      <md-progress-bar mode="indeterminate" color="primary" *ngIf="loading"></md-progress-bar>

      <router-outlet></router-outlet>
      </md-content>
  `
})
@Routes([
  {path: '/', component: Home},
  {path: '/settings',  component: Settings},
  {path: '/search',  component: Search},
  {path: '/message/:id', component: Message}
])
export class App {
  name = 'Mandrill Message Explorer';

  constructor(private router: Router, private logger: Logger) {
    logger.level = Level.DEBUG;
  }

  ngOnInit() {
  }

}
