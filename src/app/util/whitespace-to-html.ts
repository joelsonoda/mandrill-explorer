import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'mtWhitespaceToHtml'})
export class WhitespaceToHtml implements PipeTransform {
  transform(value: string): string {
    return value.replace(/\n/g, "<br>\n").replace(/\t/g, "&nbsp;&nbsp;&nbsp;&nbsp;");
  }
}