import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[mtUnsafeHref]'
})
export class UnsafeHref {
  constructor(private el: ElementRef) {}

  @Input('mtUnsafeHref') set href(href: string) {
    this.el.nativeElement.href = href;
  };
}