import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'mtUrlencode'})
export class UrlEncode implements PipeTransform {
  transform(value: string): string {
    return encodeURI(value);
  }
}