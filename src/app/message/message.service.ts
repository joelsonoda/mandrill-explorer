import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {MandrillSettings} from "../settings/settings.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class MessageService {
  constructor(private http: Http, private mandrillSettings: MandrillSettings) { }

  load(id: string): Observable<any> {
    return this.http.post(
      `https://mandrillapp.com/api/1.0/messages/content.json`,
      JSON.stringify({
        key: this.mandrillSettings.key,
        id: id
      })
    ).map(response => response.json());
  }
}