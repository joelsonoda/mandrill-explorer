import { Component } from '@angular/core';
import { MessageService } from './message.service';
import { Logger } from 'angular2-logger/core'
import { DateFormatPipe, FromUnixPipe} from 'angular2-moment';
import { OnActivate, RouteSegment } from '@angular/router';
import { WhitespaceToHtml, UrlEncode, UnsafeHref } from '../util';

@Component({
  selector: 'settings',
  pipes: [DateFormatPipe, FromUnixPipe, WhitespaceToHtml, UrlEncode],
  styles: [`
    h1 {
      font-family: Arial, Helvetica, sans-serif
    }
    
    md-card{
      margin: 25px;
    }
    
    md-input {
      width: 100%;
    }
    
    md-list > h3 {
      font-size: 1.2em;
      color: grey;
      font-weight: 100;
    }
    
    md-nav-list a[md-list-item] h3[md-line] {
      color: navy;
      font-size: .8em;
      font-weight: 600;
    }
    
    div.time-display {
      display: inline-block;
      right: 2em;
      top: 3em;
      position: absolute;
      font-weight: 100;
      color: grey;
      white-space: nowrap;
    }
    
    div.message {
      margin: 1em 1.5em;
      border: 1px solid lightgrey;
      border-radius: 2px;
      word-wrap: break-word;
    }
    
    button.selected {
      background-color: lightblue;
    }
  `],
  providers: [ MessageService ],
  directives: [ UnsafeHref ],
  template: `
  <md-card *ngIf="!loading && message">
    <md-card-header>
        <md-card-title>{{message.subject}}</md-card-title>
        <md-card-subtitle *ngIf="message.to">{{message.to.email}}<span *ngIf="message.to.name"> ({{message.to.name}})</span>
            <div class="time-display">{{(message.ts | amFromUnix) | amDateFormat: 'YYYY-MM-DD h:mmA'}}</div>
        </md-card-subtitle>
    </md-card-header>
    <md-card-content>
        <button md-raised-button [ngClass]="{selected: messageHtml}" (click)="messageHtml=true;">HTML</button>
        <button md-raised-button [ngClass]="{selected: !messageHtml}" (click)="messageHtml=false;">TEXT</button>
        <div class="message" [innerHTML]="message.html" *ngIf="messageHtml"></div>
        <div class="message" *ngIf="!messageHtml" style="padding: .5em;" [innerHTML]="message.text | mtWhitespaceToHtml"></div>
    </md-card-content>
    <md-card-content *ngIf="message.attachments && message.attachments.length > 0">
        <a *ngFor="let attachment of message.attachments" 
            md-raised-button
            download="{{attachment.name}}"
            [mtUnsafeHref]="attachmentData(attachment)">
            {{attachment.name}}
        </a>
    </md-card-content>
  </md-card>
  <md-card *ngIf="loading">
    <md-progress-circle mode="indeterminate" color="primary"></md-progress-circle>
  </md-card>
  <md-card *ngIf="!loading && !message">
    <h3><md-icon>report problem</md-icon>No Messages Found</h3>
  </md-card>
  `
})
export class Message implements OnActivate {
  private message: any;
  private loading: boolean = false;
  private messageHtml: boolean = true;

  constructor(private messageSearchService: MessageService, private logger: Logger) {}

  attachmentData(attachment: any): string {
    if(attachment) {
      return `data:${attachment.type};base64,${encodeURIComponent(attachment.content)}`;
    } else {
      return null;
    }
  }

  routerOnActivate(curr: RouteSegment): void {
    let id: string = curr.getParam('id');
    this.loading = true;
    this.messageSearchService.load(id).subscribe( message => {
      this.message = message;
      this.loading = false;
    });
  }

}
