import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import {MandrillSettings} from "../settings/settings.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class MessageSearchService {
  constructor(private http: Http, private mandrillSettings: MandrillSettings) { }

  search(query: string): Observable<any[]> {
    return this.http.post(
      `https://mandrillapp.com/api/1.0/messages/search.json`,
      JSON.stringify({
        key: this.mandrillSettings.key,
        query: query
      })
    ).map(response => response.json());
  }
}
