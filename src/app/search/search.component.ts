import { Component } from '@angular/core';
import { MessageSearchService } from './search.service';
import { Logger } from 'angular2-logger/core'
import {DateFormatPipe, FromUnixPipe} from 'angular2-moment';
import { Router } from '@angular/router';


@Component({
  selector: 'settings',
  pipes: [DateFormatPipe, FromUnixPipe],
  styles: [`
    h1 {
      font-family: Arial, Helvetica, sans-serif
    }
    
    md-card{
      margin: 25px;
    }
    
    md-input {
      width: 100%;
    }
    
    md-list > h3 {
      font-size: 1.2em;
      color: grey;
      font-weight: 100;
    }
    
    md-nav-list a[md-list-item] h3[md-line] {
      color: navy;
      font-size: .8em;
      font-weight: 600;
    }
    
    div.time-display {
      display: block;
      right: 0;
      position: relative;
      font-size: .8em;
      font-weight: 100;
      color: grey;
      white-space: nowrap;
    }
  `],
  providers: [ MessageSearchService ],
  template: `
  <md-card>
    <form>
      <md-input placeholder="Enter Your Query" [(ngModel)]="query" autofocus></md-input>
      <button type="submit" md-raised-button color="primary" (click) = "search(query)">Search</button>
    </form>
  </md-card>
  <md-card *ngIf="!loading && messages && messages.length > 0">
    <md-nav-list>
        <h3>Emails Sent</h3>
        <template ngFor let-message [ngForOf]="messages" let-first="first">
          <md-divider *ngIf="!first"></md-divider>
          <a md-list-item (click)="router.navigate(['/message',message._id])">
            <h3 md-line>{{message.sender}} ➔ {{message.email}}</h3>
            <p md-line>{{message.subject}}</p>
            <div class="time-display">{{(message.ts | amFromUnix) | amDateFormat: 'YYYY-MM-DD h:mmA'}}</div>
          </a>
        </template>
    </md-nav-list>
  </md-card>
  <md-card *ngIf="loading">
    <md-progress-circle mode="indeterminate" color="primary"></md-progress-circle>
  </md-card>
  <md-card *ngIf="!loading && messages && messages.length == 0">
    <h3><md-icon>report problem</md-icon>No Messages Found</h3>
  </md-card>
  `
})
export class Search {
  private messages: any[];
  private loading: boolean = false;

  constructor( private messageSearchService: MessageSearchService, private logger: Logger, private router: Router ) {

  }

  ngOnInit() {
  }

  search(query: string) {
    this.loading = true;
    this.logger.debug(`Your search term is ${query}`);
    this.messageSearchService.search(query).subscribe( messages => {
      this.messages = messages;
      this.loading = false;
    });
  }

}
