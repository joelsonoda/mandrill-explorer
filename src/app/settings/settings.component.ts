import { Component } from '@angular/core';
import { MandrillSettings } from './settings.service'

@Component({
  selector: 'settings',
  styles: [`
    h1 {
      font-family: Arial, Helvetica, sans-serif
    }
    md-card{
      margin: 25px;
    }
    md-input {
      width: 100%;
    }
  `],
  providers: [ ],
  template: `
  <md-card>
    <md-input
          placeholder="Specify Your Mandrill API Key"
          [(ngModel)]="mandrillSettings.key"
          autofocus>
    </md-input>
  </md-card>

  `
})
export class Settings {
  constructor(private mandrillSettings: MandrillSettings) {

  }

  ngOnInit() {
  }

}
