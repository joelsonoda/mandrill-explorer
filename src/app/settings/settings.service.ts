import { Injectable } from '@angular/core';

@Injectable()
export class MandrillSettings {

  constructor() {}

  get key() {
    let key = localStorage.getItem('MANDRILL_KEY');
    if(key) {
      return key;
    } else {
      return null;
    }
  }

  set key(key) {
    localStorage.setItem('MANDRILL_KEY', key);
  }
}
