import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MandrillSettings} from "../settings/settings.service";
@Component({
  template: ''
})
export class Home implements OnInit {

  constructor(private router: Router, private mandrillSettings: MandrillSettings) { }

  ngOnInit() {
     if(this.mandrillSettings.key) {
       this.router.navigate(['/search']);
     } else {
       this.router.navigate(['/settings']);
     }
  }
}
