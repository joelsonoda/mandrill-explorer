# Mandrill Explorer

This tool is meant to make testing while using the [mandrill transactional API](https://mandrillapp.com) more accessible by

* Providing a way for developers to view the content of sent messages using an API key rather than a mailchimp login
* View the attachments that were sent along with a message
 
I wanted developers to be able to see the email that their application generates without having to create mailchip accounts for everyone. With this tool, developers can use the same key that they use for sending email in development mode to view the email that would have been sent (since there is no deliverability when sending with a test key).

In addition, when I would log in and view content via the mandrill web app, I still found no way of viewing the attachments that were generated along with the email. A critical part of our email testing in development mode is to make sure that we are sending the appropriate attachments.

I created this simple project that generates a static site allowing a developer to use an HTML5 compatible browser to view messages sent with mandrill and download the attachments.

To run in development mode to contribute to this client, you'll need to do the following:
1. install webpack, tsc and typings globally.
```sh
npm install -g webpack tsc typings
```
2. npm install
```sh
npm install
```
3. Run the server in development mode
```sh
npm run server:dev:hmr
```

This project is based on the [Angular2 Webpack Starter](https://angularclass.github.io/angular2-webpack-starter/) from [AngularClass](https://angularclass.com/) and is still permeated with traces of their work.